import { TaskListItemView } from "./TaskListItemView.js"
import { Collection, CollectionEvent, TasksCollection } from "./TasksCollection.js"

export class TasksView {

  /** @type {TasksCollection} */
  collection
  constructor(el = document.querySelector('#tasks-list')){
    this.el = el
  }

  /** @type {Collection} */
  listenTo(collection) {
    this.collection = collection
    // this.collection.addEventListener('reset', this.handleReset.bind(this))
    this.collection.addEventListener('reset', (e) => this.handleReset(e))
  }

  handleReset(/** @type {CollectionEvent} */ event) {
    // event.currentTarget.getAll() 
    this.render()
  }

  itemFactory = TaskListItemView

  render() {
    while (this.el.firstElementChild)
      this.el.firstElementChild.__view?.destroy()

    this.collection.getAll().forEach(task => {
      const taskItemView = new this.itemFactory()
      taskItemView.listenTo({ task })
      taskItemView.appendTo(this.el)
    })

  }
}
