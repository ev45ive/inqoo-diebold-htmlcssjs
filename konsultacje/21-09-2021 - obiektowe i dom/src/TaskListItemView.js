
export class TaskListItemView {
    /** @type {HTMLDivElement} */ el;
    /** @type {import("./TasksCollection.js").Task} */ model;

  render() {
    this.el = this.el || document.createElement('div');
    this.el.__view = this;

    this.el.classList.add('list-group-item');
    this.el.dataset.taskId = this.model.task.id;

    /* es6-string-html - wtyczka */
    this.el.innerHTML = /* html */ `
      <input type="checkbox" ${this.model.task.completed ? 'checked' : ''} />
      <span>${this.model.task.title}</span>`;
  }

  destroy() {
    // ... cleanup, say goodbye..
    console.log('good bye!')
    this.el.remove()
  }

  listenTo(model) {
    this.model = model;
  }

  appendTo(listEl) {
    this.render();
    listEl.appendChild(this.el);
  }
}


export class TaskListItemView2 extends TaskListItemView {
  constructor() { super() }
  render() {
    super.render()
    this.el.innerHTML = /* html */ `
      <span>${this.model.task.title}</span>
      <input type="checkbox" ${this.model.task.completed ? 'checked' : ''} />
    `;
  }

}