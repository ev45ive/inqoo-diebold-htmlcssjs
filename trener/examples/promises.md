```js
function echo(msg){
    return new Promise((resolve)=>{        
        setTimeout(()=>{
            resolve(msg)
            console.log('done')
        }, 2000)
    })
};

p = echo('test')
p.then(console.log)

```

```js
function echo(msg){
    return new Promise((resolve)=>{        
        setTimeout(()=>{
            resolve(msg)
//             console.log('done')
        }, 2000)
    })
};

p = echo('test')

// multiple callbacks
p.then(console.log)
p.then(console.log)
p.then(console.log)

// late callback
setTimeout(()=>{ p.then(console.log) },3000)

// promise chain
p2 = p.then( res => res + ' placki')
p2.then(res => console.log(res)) 

Promise {<pending>}
// ... 3sec
test
test
test

test placki

// ... 1sec
test
```


```js

//  echo('test')
// .then( res => echo( res + ' placki')
//                     .then(res => console.log(res)))
 echo('test')
.then( res => echo( res + ' placki'))
.then( res => console.log(res) )

/// 2sec + 2sec...
test placki
```

## Error handling
```js
function echo(msg,error){
    return new Promise((resolve,reject)=>{        
        setTimeout(()=>{
            error? reject(error) : resolve(msg)
        }, 2000)
    })
};

echo('Alice',' Nie ma nikogo ')
    .then( res => echo(res + ' ma '), err => 'Anonim nie ma ')
    .then( res => echo(res + ' kota ', 'upss..'))
    .catch( err => Promise.reject(err+ 'Nie ma niczego') )
    .then(console.log) 

// Promise {<pending>}
// localhost/:1 Uncaught (in promise) upss..Nie ma niczego
```

## All
```js
function echo(msg,time, error){
    return new Promise((resolve,reject)=>{        
        setTimeout(()=>{
            error? reject(error) : resolve(msg)
        }, time)
    })
};

p = Promise.all([
    echo(1,1500),
    echo(2,Math.random() * 2000),
    echo(3,500, 'upss'),
])
p.finally(console.log)
p.then(console.log, console.error) 


```

## What promise cannot do?
- retry
- finish
- cancel


